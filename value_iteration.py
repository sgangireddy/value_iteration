'''
Created on 24 Aug 2016

@author: Siva Reddy Gangireddy
'''

import numpy, itertools

class POMDPVarLoad:


    def __init__(self, file_name):
        '''
        Constructor
        '''
        f = open(file_name, 'r') #opens the file with pomdop variables: transistion, observation and reward probabilities
        self.variables = [] #list to hold the variables in raw from
        for line in f:
            if (not (line.startswith('#') or line.isspace())): #discards the line starts with '#' tag
                self.variables.append(line.strip())
                
        self.tran_prob = {}  #hash for transistion probabilties
        self.obs_prob = {}   #hash for observation probabilties
        self.reward_fun = {} #hash for rewards, given an action
        
        for var in self.variables:
            if var.startswith("discount"):
                self.discount = float(var.split()[1])
            elif var.startswith("values"):
                self.values = var.split()[1]
            elif var.startswith("states"):
                self.states = var.split()[1:]
            elif var.startswith("actions"):
                self.actions = var.split()[1:]
            elif var.startswith("observations"):
                self.observations = var.split()[1:]
            elif var.startswith("T"):
                self.get_tran_prob(var)
            elif var.startswith("O"):
                self.get_obs_prob(var)
            else:
                self.get_reward_fun(var)
        
    def get_tran_prob(self, var): #load the transistion probabilities into hash, tran_pob
        var = var.split()
        elements = []
        for ele in var:
            if not ele == ':':
                elements.append(ele)
        action = elements[1]
        start_state = elements[2]
        end_state = elements[3]
        prob = float(elements[4])
        self.tran_prob[(action, start_state, end_state)] = prob
        
    def get_obs_prob(self, var): #load the observation probabilties into hash, obs_prob
        var = var.split()
        elements = []
        for ele in var:
            if not ele == ':':
                elements.append(ele)
        action = elements[1]
        end_state = elements[2]
        observation = elements[3]
        prob = float(elements[4])
        self.obs_prob[(action, end_state, observation)] = prob        
    def get_reward_fun(self, var): #load the rewards into hash, reward_fun
        var = var.split()
        elements = []
        for ele in var:
            if not ele == ':':
                elements.append(ele)
        action = elements[1]
        state = elements[2]
        reward = float(elements[3])
        self.reward_fun[(action, state)] = reward

class POMDPPolicyLearn:
    
    def __init__(self, discount, values, states, actions, obs, tran_prob, obs_prob, reward_fun, T, step_val):
        self.discount = discount
        self.values = values
        self.states = states
        self.actions = actions
        self.observations = obs
        self.tran_prob = tran_prob
        self.obs_prob = obs_prob
        self.reward_fun = reward_fun
        self.step_val = step_val
        self.T = T #number of time steps 
        self.t = 1 # to start with, one step conditional palns
        self.N = 1 #number of (t-1)-step conditional plans, this N changes over the time
        self.V = [[0, 0]] #value of conditional plan at t=0, V(s) = [0,0]
        self.v = {} #to hold the intermediate values, during computation
        self.K = []
        
    def policy_learn(self):
        for t in xrange(self.t, self.T+1):
            if (t%10) == 0:
                print "At t = %d....." %(t)
            self.K = self.set_operations() #Cartesian product of V, step5 in the algorithm, page15
            for action in self.actions:             #iterates over the actions
                for k in self.K:
                    for state in self.states:       #iterates over hidden states
                        reward = self.reward_fun[(action, state)] #reward for given action and end-state
                        sum = 0
                        for sindex_, state_ in enumerate(self.states):
                            for oindex_, obs_ in enumerate(self.observations):
                               sum += (self.tran_prob[(action, state, state_)] * self.obs_prob[(action, state_, obs_)] * k[oindex_][sindex_]) #future expected value, step9, page15
                        if not self.v:
                            self.v[(action, str(k))] = []
                            self.v[(action, str(k))].append(reward + (self.discount*sum))
                        elif (action, str(k)) in self.v:
                            self.v[(action, str(k))].append(reward + (self.discount*sum))
                        else:
                            self.v[(action, str(k))] = []
                            self.v[(action, str(k))].append(reward + (self.discount*sum))
            self.V, action_pi= self.optimal_plans(t)
        f = open("pomdp_policy", 'w')
        for V_, a_pi in zip(self.V, action_pi):
            f.write("%s %s %s\n" %(a_pi, V_[0], V_[1]))
            

    def optimal_plans(self, t): #step11 to step17, page 15
        plans  = []
        best_plan_index = []
        best_plan = []
        best_action = []
        
        if (t%10) == 0:
            print "    Pruning the V(s)..."
        for action in self.actions:
            for k in self.K:
                plans.append(self.v[(action, str(k))]) #consits of all possible V(s) (unpruned), at time step 't'
                #Here V(s), unpruned are in the following order
                #(action, K)
                #for pomdp_conversation
                #(ask, k1)
                #(ask, k2)
                #....
                #(ask, |K|)
                #(doSave, k1)
                #...
                #(doSave, |K|)
                #(doDelete, k1)
                #....
                #(doDeletem |K|)
    
        for b_ in numpy.arange(0, 1, self.step_val): #sampling (1/self.step_val) points from belief state space, to get the best plans (V(s)) and actions
            plan_belief = []
            for p_ in plans:
                plan_belief.append((((1-b_)*p_[0]) + (b_*p_[1]))) #computation of V(b) with sampled 'b' points/vectors from belief state space

            plan_belief = numpy.array(plan_belief)    #to get the argmax, converting vector of V(b) into a array
            max_index = plan_belief.argmax()          #index of best plan V(s) at current belief state
            best_plan_index.append(max_index)         #holds the indicies of best plans (pruned V(s)), at current belief state [1, 3,....5]
            best_plan.append(plans[max_index])        #holds the best V(s), at current belief state [[], [], ....]
            
            if max_index >=0 and max_index <= len(self.K)-1:
                best_action.append("ask")
            elif max_index >=len(self.K) and max_index <= (2*len(self.K))-1:
                best_action.append("doSave")
            else:
                best_action.append("doDelete")                
                
        best_plan_, best_action_ = [], []
        for bp, ba in zip(best_plan, best_action):
            if not best_plan_:
                best_plan_.append(bp)
                best_action_.append(ba)
            elif bp in best_plan_:
                continue
            else:
                best_plan_.append(bp)
                best_action_.append(ba)
        if (t%10) == 0:
            print "    Number of best V(s) = %d" %(len(best_plan_))
        return best_plan_, best_action_
            
        
    def set_operations(self): #returns the cartesian product of two sets
        V =self.V
        cart_prod = itertools.product(V,V)
        cart_prod_list = []
        for ele in cart_prod:
            cart_prod_list.append(ele)
        return cart_prod_list
    
class POMDPConversation():
    
    def __init__(self, states, tran_prob, obs_prob, policy_file_name, initial_belief):
        self.states = states
        self.tran_prob = tran_prob
        self.obs_prob = obs_prob
        self.policy_file_name = policy_file_name
        self.belief = initial_belief
    
        f = open(self.policy_file_name, 'r')
        lines = f.readlines()
        self.actions = {}
        self.policy = []
        for i, line in enumerate(lines):
            tmp = []
            line = line.strip().split()
            self.actions[i] = line[0]
            tmp.append(float(line[1]))
            tmp.append(float(line[2]))
            self.policy.append(tmp)
        self.policy = numpy.array(self.policy)
            
    def reward_value_given_belief(self):   #Returns the maximum expected reward and the corresponding action, given belief state
        reward_values = self.policy.dot(numpy.array(self.belief)) #dot product between belief vector and policy matirx
        max_reward = reward_values.max()
        max_reward_index = reward_values.argmax()
        best_action = self.actions[max_reward_index]
        return max_reward, best_action
    
    def update_belief(self, action, observation_recived): #updates the belief after receiving the observation from the user
        new_belief = []
        for state in self.states: #iterates over next set of states
            o_prob = self.obs_prob[(action, state, observation_recived)]
            sum = 0.0
            for i, state_ in enumerate(self.states):
                sum += self.tran_prob[(action, state_, state)] * self.belief[i] #tran_prob[(action, start_state, end_state)]
            new_belief.append(sum*o_prob)
        norm = new_belief[0] + new_belief[1]   #computing the denominator of Equation 2.2, in Page6
        for i, b_ in enumerate(new_belief):
            new_belief[i] = new_belief[i]/norm    #normalisation term
        self.belief = new_belief
