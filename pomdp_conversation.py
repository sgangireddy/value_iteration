'''
Created on 26 Aug 2016

@author: Siva Reddy Gangireddy
'''

from value_iteration import *
import os.path


#Load the pomdp variables
print "Loading POMDP variables from file...."
pomdp = POMDPVarLoad("pomdp_variables")

policy_file_name = "pomdp_policy"
T = 100 #number of time steps OR number of iterations
step_val = 0.01 #step value to sample the belief space

if not os.path.isfile(policy_file_name):
    print "Value iteration algorithm..."
    #Initialising the PolicyLearn class
    pomdp_policy = POMDPPolicyLearn(pomdp.discount, pomdp.values, pomdp.states, pomdp.actions, pomdp.observations, pomdp.tran_prob, pomdp.obs_prob, pomdp.reward_fun, T, step_val)
    #value iteration algorithm
    pomdp_policy.policy_learn()
else:
    print "Policy file exists.." , "Jumping to pomdp conversation"

#pomdp conversation with machine
#Task is to save the message: Once the message is saved the belief state transitions back to initial belief state, b0
initial_belief = [0.65, 0.35] #b0
pomdp_conversation = POMDPConversation(pomdp.states, pomdp.tran_prob, pomdp.obs_prob, policy_file_name, initial_belief)
observations = ["hearDelete", "hearSave", "hearSave", "hearSave"]
#observations = ["hearSave", "hearDelete", "hearDelete", "hearDelete"]

print "Starting conversation with the machine...."
print "With initial belief :%s" %(initial_belief)
print "\n",
index = 0
while 1:
    max_reward, best_action = pomdp_conversation.reward_value_given_belief()
    if best_action == "ask":
        pomdp_conversation.update_belief(best_action, observations[index])
        new_belief = pomdp_conversation.belief
        print "optimal action:", best_action,"\t" "reward:%f" %(max_reward), "\t", "new_belief_state:" , new_belief
        index = index + 1
    else:
        print "the optimal action is: %s" %(best_action)
        break
    
pomdp_conversation.update_belief(best_action, observations[index])
new_belief = pomdp_conversation.belief
print "optimal action:", best_action,"\t" "reward:%f" %(max_reward), "\t", "new_belief_state:" , new_belief
print "\n"     