'''
Created on 27 Aug 2016

@author: Siva Reddy Gangireddy
'''

import matplotlib.pyplot as plt
import numpy

policy_file_name = "pomdp_policy"

f = open(policy_file_name, "r")

lines = f.readlines()
conditional_plan = []
#line: ask 5.24283532483 0.0894013711154
for i, line in enumerate(lines):
    tmp = []
    line = line.strip().split()
    tmp.append(float(line[1]))
    tmp.append(float(line[2]))
    conditional_plan.append(tmp) #[[], [], []....[]]

plt.figure()
index = numpy.arange(0, 1.01, 0.01)
    
for plan in conditional_plan: #[[], [], ....[]]
    plan_belief = []
    for b_ in numpy.arange(0, 1.01, 0.01): #[0, 0.01, ....0.99, 1.00]
        plan_belief.append(((1-b_)*plan[0]) + (b_*plan[1]))
    plt.plot(index, plan_belief, 'K')
ax1 = plt.axes()
ax1.get_xaxis().set_visible(False)
right_yaxis = plt.twinx()
right_yaxis.set_ylim(-20, 10)
plt.savefig("conditional_plan.pdf")
#plt.show()